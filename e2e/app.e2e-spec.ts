import { WeatherApiPage } from './app.po';

describe('weather-api App', () => {
  let page: WeatherApiPage;

  beforeEach(() => {
    page = new WeatherApiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
