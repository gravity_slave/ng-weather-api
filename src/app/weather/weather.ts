export interface IWeather extends Object {
  name: string;
  description: string;
  temperature: number;
}
