import { Injectable } from '@angular/core';
import {Observable } from 'rxjs/Observable';
import { IWeather } from './weather';
import { WEATHER_ITEMS } from './weather.data';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class WeatherService {
  private URL = 'http://api.openweathermap.org/data/2.5/weather?q=';
  private KEY = '3a1a93785099ab90e2f64fe09ee19288';
  private IMP = '&units=imperial';

  constructor(private _http: HttpClient) { }

  getWeatherItems() {
    return WEATHER_ITEMS;
  }

  addWeatherItem(weatherItem: IWeather) {
    WEATHER_ITEMS.push(weatherItem);
    console.log(WEATHER_ITEMS);
  }

  searchWeatherData(cityName: string): Observable<IWeather[]> {
    return this
      ._http
      .get(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`, {
        headers: new HttpHeaders(),
      })
      .map((res) => <IWeather[]> res)
      .do(res => console.log('Weather Data Object ' + JSON.stringify(res)))
      .catch(error => {
        console.error(error);
        return Observable.throw(error.json())
      });
  }
}
