import { Component, OnInit } from '@angular/core';
import { IWeather } from '../weather';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {
  weatherItmes: IWeather[];
  constructor(private _weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeatherItems();
  }

  getWeatherItems() {
    this.weatherItmes = this._weatherService.getWeatherItems()
  }

}
