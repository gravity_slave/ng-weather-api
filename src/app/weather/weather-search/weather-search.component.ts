import {Component, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { WeatherService } from '../weather.service';
import { IWeather } from '../weather';

@Component({
  selector: 'app-weather-search',
  templateUrl: './weather-search.component.html',
  styleUrls: ['./weather-search.component.css']
})
export class WeatherSearchComponent implements OnInit {
  searchInput: FormControl = new FormControl('');
  city: IWeather;

  constructor(private _weatherService: WeatherService) {  }

  ngOnInit() {
    this.searchInput.valueChanges
      .debounceTime(1000)
      .distinctUntilChanged()
      .switchMap((input: string) => this._weatherService.searchWeatherData(input))
      .map(city => city[0])
      .map(city => city as IWeather)
      .subscribe( city => this.city = city,
        err => {
          console.log(`Can't get weather. Error code: ${err.code}.Message: ${err.message} `);
          console.log(err)
        })
  }

  onSubmit() {
    const weatherItem: IWeather = {
      name: this.city.name,
      description: this.city.weather[0].description,
      temperature: this.city.main.temp
    }
    console.log(weatherItem);
    this.addWeatherItem(weatherItem);
  }

  addWeatherItem(item: IWeather) {
    this._weatherService.addWeatherItem(item);
    this.searchInput.reset();
  }
}
